// Copyright (C) Roel Standaert
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "dsl.hpp"
#include "util.hpp"

namespace StandaertHA::DSL {

constexpr Context::Context(const ButtonEvent &currentEvent,
                           uint32_t outputBefore)
  : currentEvent_(currentEvent),
    outputBefore_(outputBefore),
    outputAfter_(outputBefore)
{ }

constexpr Event Context::on(ButtonEvent evt)
{
  return Event(*this, evt);
}

constexpr Event Context::onPressStart(uint8_t button)
{
  return Event(*this, ButtonEvent(button, ButtonEvent::Type::PressStart));
}

constexpr Event Context::onPressEnd(uint8_t button)
{
  return Event(*this, ButtonEvent(button, ButtonEvent::Type::PressEnd));
}

constexpr Event::Event(Context &ctx,
                       ButtonEvent onEvent)
  : ctx_(ctx),
    onEvent_(onEvent)
{ }

constexpr Event Event::toggle(uint8_t output) &&
{
  if (onEvent_ != ctx_.currentEvent_)
    return *this;
  
  const int before = getBit(ctx_.outputBefore_, output);
  if (before == HIGH) {
    setBit(ctx_.outputAfter_, output, LOW);
  } else {
    setBit(ctx_.outputAfter_, output, HIGH);
  }

  return *this;
}

constexpr Event Event::turnOn(uint8_t output) &&
{
  if (onEvent_ != ctx_.currentEvent_)
    return *this;

  setBit(ctx_.outputAfter_, output, HIGH);

  return *this;
}

constexpr Event Event::switchOff(uint8_t output) &&
{
  if (onEvent_ != ctx_.currentEvent_)
    return *this;

  setBit(ctx_.outputAfter_, output, LOW);

  return *this;
}

}
